use Mix.Config

# Configure your database
config :fh_schedule, FhSchedule.Repo,
  username: "postgres",
  password: "postgres",
  database: "fh_schedule_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :fh_schedule, FhScheduleWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
