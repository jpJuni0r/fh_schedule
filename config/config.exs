# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :fh_schedule,
  ecto_repos: [FhSchedule.Repo]

# Configures the endpoint
config :fh_schedule, FhScheduleWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "zElOo9cuweABwukL+KRg4UJuvivZSS2oZ2E5vQzIUHJUI65Z3A81kFvKxpLBXEdV",
  render_errors: [view: FhScheduleWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: FhSchedule.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [
    signing_salt: "7ZYqWCxw+S9pLzdvQ8g5S8OPsnRPw4og"
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
