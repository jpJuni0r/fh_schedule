defmodule FhScheduleWeb.Router do
  use FhScheduleWeb, :router
  import Phoenix.LiveView.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug Phoenix.LiveView.Flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", FhScheduleWeb do
    pipe_through :browser

    live "/", CalendarLive
    get "/kalender", ICalendarController, :show
    live "/counter", CounterLive
    get "/home", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", FhScheduleWeb do
  #   pipe_through :api
  # end
end
