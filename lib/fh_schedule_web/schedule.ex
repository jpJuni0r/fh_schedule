defmodule FhScheduleWeb.Schedule do
  @start ~T[08:00:00]
  @class_length 75
  @pause_length 15

  def new() do
    # [{~T[08:00:00.000000], ~T[09:15:00.000000]}, ...]
    courses = []
    times =
      0..6
      |> Enum.map(fn index -> [index, Time.add(@start, index * (@class_length + @pause_length) * 60, :second)] end)
      |> Map.new(fn [index, start] -> {index, {start, Time.add(start, @class_length * 60, :second), courses}} end)

    0..6
    |> Map.new(fn day -> {day, times} end)
  end

  @doc ~S"""
  Adds courses to a schedule

  ## Example

      iex> FhScheduleWeb.Schedule.new() |> FhScheduleWeb.Schedule.add_courses([%{day: 0, start: ~T[08:00:00], name: "My Course"}])
      %{
        0 => %{
          0 => {~T[08:00:00.000000], ~T[09:15:00.000000],
              [%{day: 0, name: "My Course", start: ~T[08:00:00]}]},
          1 => {~T[09:30:00.000000], ~T[10:45:00.000000], []},
          2 => {~T[11:00:00.000000], ~T[12:15:00.000000], []},
          ...
        },
        ...
      }
  """
  def add_courses(calendar, courses) do
    courses
    |> Enum.reduce(calendar, fn course, calendar ->
      Map.update!(calendar, course.day, fn time_slot ->
        Map.update!(time_slot, get_time_slot(course.start), fn {startTime, endTime, courses} ->
          {startTime, endTime, [course | courses]}
        end)
      end)
    end)
  end

  @doc ~S"""
  For a given time it calculates the index of the course of a day

  ## Example

      iex> FhScheduleWeb.Schedule.get_time_slot(~T[08:00:00])
      0
      iex> FhScheduleWeb.Schedule.get_time_slot(~T[14:00:00])
      4
  """
  def get_time_slot(time) do
    minute_start = @start.hour * 60 + @start.minute
    offset = time.hour * 60 + time.minute - minute_start
    div(offset, (@class_length + @pause_length))
  end

  def weekday_to_string(weekday) do
    0..6
    |> Enum.zip(~w(Montag Dienstag Mittwoch Donnerstag Freitag Samstag Sonntag))
    |> Map.new()
    |> Map.get(weekday)
  end

  def deduplicate(list) do
    list
    |> Enum.reduce([], fn item, acc ->
      if Enum.at(acc, 0) == item, do: acc, else: [item | acc]
    end)
    |> Enum.reverse()
  end
end
