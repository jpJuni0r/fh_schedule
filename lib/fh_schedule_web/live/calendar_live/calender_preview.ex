defmodule FhScheduleWeb.CalendarLive.CalendarPreview do
  import Phoenix.HTML, only: [sigil_E: 2]
  alias FhScheduleWeb.Schedule
  alias Timex

  def render(courses, selected_courses) do
    cal =
      Schedule.new()
      |> Schedule.add_courses(
        courses |> Enum.filter(fn course -> Map.has_key?(selected_courses, course.name) end)
      )
    ~E"""
    <div class="flex-1">
      <div class="grid m-2">
        <%= for {hour, {startTime, _endTime, _}} <- Map.get(cal, 0) do %>
          <%= render_cell(startTime |> Timex.format!("%H:%M", :strftime), 1, hour + 2) %>
        <% end %>

        <%= for {day, day_schedule} <- cal do %>
          <%= render_cell(day |> Schedule.weekday_to_string() , day + 2, 1) %>

          <%= for {hour, {_startTime, _endTime, courses}} <- day_schedule do %>
            <%= render_cell(courses, day + 2, hour + 2) %>
          <% end %>
        <% end %>
      </div>
    </div>
    """
  end

  def render_cell(content, column, row) when is_list(content) do
    text =
      content
      |> Enum.reverse()
      |> Enum.map(fn course -> course.name end)
      |> Schedule.deduplicate()
      |> Enum.join(", ")
    render_cell(text, column, row)
  end

  def render_cell(content, column, row) do
    ~E"""
    <div style="grid-column: <%= column %>; grid-row: <%= row %>">
      <%= content %>
    </div>
    """
  end
end
