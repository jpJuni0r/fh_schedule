defmodule FhScheduleWeb.CalendarLive do
  use Phoenix.LiveView
  import Phoenix.HTML.Form
  import Phoenix.HTML.Tag, only: [content_tag: 3]
  import FhScheduleWeb.StyledForm
  alias FhSchedule.{Course, CourseSelection, Major}
  alias FhScheduleWeb.CalendarLive.{SearchForm, CalendarPreview}

  def render(assigns) do
    count = map_size(assigns.selected_courses)
    ~L"""
    <%= if @course_selection do %>
      <div class="bg-blue-100 border border-blue-400 text-blue-700 px-4 py-3 rounded relative my-2 mx-auto max-w-md" role="alert">
        <strong class="font-bold">Kalender erstellt!</strong>
        <span class="block sm:inline">Dein Kalender wurde erstellt und ist <a href="/kalender?id=<%= @course_selection.id%>" target="_blank" class="underline">hier</a> einsehbar. Füge ihn in deinen Kalender ein. </span>
      </div>
    <% end %>
    <div class="flex">
      <div class="max-w-sm rounded overflow-hidden shadow-lg bg-white m-2">
        <div class="px-6 py-4">
          <div class="font-bold text-xl mb-2 text-gray-800">
            Kurse zusammenstellen
          </div>

          <%= f = form_for @changeset, "#", [phx_change: :change, phx_submit: :submit] %>
            <div class="mb-6">
              <%= styled_label f, :searchterm, "Suchbegriff" %>
              <%= styled_text_input f, :searchterm, placeholder: "Mathematik, Rechnernetze, ..." %>
            </div>

            <div class="mb-6">
              <%= styled_label f, :major, "Studiengang" %>
              <div class="relative">
                <select class="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                  <%= for major <- @majors do %>
                    <option><%= major.name %></option>
                  <% end %>
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700"><svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"></path></svg></div>
              </div>
            </div>

            <div class="mb-6">
              <%= styled_label f, :selected_courses, "Kurse#{if count > 0, do: " (#{count})"}" %>

              <div class="h-64 overflow-auto border border-gray-400 px-4 py-2 pr-8 rounded shadow">
                <%= render_courses(@courses_search, @selected_courses) %>
              </div>
            </div>

            <div>
              <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                Absenden
              </button>
            </div>
          </form>
        </div>
      </div>

      <%= CalendarPreview.render(@courses, @selected_courses) %>
    </div>
    """
  end

  def mount(_session, socket) do
    {:ok, new_session(socket)}
  end

  def handle_event("dec", _, socket) do
    {:noreply, update(socket, :val, &(&1 - 1))}
  end

  def handle_event("change", %{"search_form" => opts}, socket) do
    changeset = SearchForm.changeset(%SearchForm{}, opts)
    courses_search = Course.seach_courses_distinct(opts["searchterm"])
    socket =
      socket
      |> assign(:changeset, changeset)
      |> assign(:courses_search, courses_search)
    {:noreply, socket}
  end

  def handle_event("submit", _, %{:assigns => %{:selected_courses => courses}} = socket) do
    changeset = CourseSelection.changeset(%CourseSelection{}, %{courses: courses |> Map.keys()})
    {:ok, course_selection} = CourseSelection.create_course_selection(changeset)
    {:noreply, assign(socket, :course_selection, course_selection)}
  end

  def handle_event("toggle", %{"name" => name}, socket) do
    # toggles the `name` key in map
    selected_courses = if Map.has_key?(socket.assigns.selected_courses, name) do
      Map.delete(socket.assigns.selected_courses, name)
    else
      Map.put(socket.assigns.selected_courses, name, true)
    end
    {:noreply, assign(socket, :selected_courses, selected_courses)}
  end

  defp new_session(socket) do
    courses = Course.list_distinct_courses()
    majors = Major.list_majors()
    assign(socket, %{
      courses: courses,
      courses_search: courses,
      selected_courses: %{"Analysis" => true},
      majors: majors,
      changeset: SearchForm.changeset(%SearchForm{}, %{}),
      course_selection: nil,
    })
  end

  defp render_courses(courses, selected_courses) do
    items =
      courses
      |> Enum.reduce([], fn course, acc ->
        prev = Enum.at(acc, 0)
        if prev != nil && prev.name == course.name do
          acc
        else
          [course | acc]
        end
      end)
      |> Enum.reverse()
      |> Enum.reduce([], fn course, acc ->
        item = {:course, course}
        # add divider when semester of this course is not not same as the previous
        draw_divider = fn -> [item | [{:divider, course.semester} | acc]] end
        if acc == [] do
          draw_divider.()
        else
          case Enum.at(acc, 0) do
            {:divider, _} ->
              [item | acc]
            {:course, %{:semester => semester}} ->
              if semester == course.semester do
                [item | acc]
              else
                draw_divider.()
              end
          end
        end
      end)
      |> Enum.reverse()

    for item <- items do
      case item do
        {:course, course} ->
          type = if Map.has_key?(selected_courses, course.name) do
            :active
          else
            :default
          end
          render_course(course, type: type)
        {:divider, text} ->
          render_divider(text)
      end
    end
  end

  defp render_divider(semester) do
    content_tag :div, "Semester #{semester}", class: "py-1 px-1 mb-1 text-gray-700 text-sm font-bold"
  end

  defp render_course(%{:name => name}, opts) do
    base_classes = "border border-white rounded cursor-default py-1 px-3 mb-1"
    class = case opts[:type] do
      :active -> "#{base_classes} border-blue-500 bg-blue-500 text-white"
      :default -> "#{base_classes} hover:border-gray-200 text-blue-500 hover:bg-gray-200"
      _ -> throw "Unknown key"
    end
    opts =
      opts
      |> Keyword.delete(:type)
      |> Keyword.put(:class, class)
      |> Keyword.put(:phx_click, "toggle")
      |> Keyword.put(:phx_value_name, name)
    content_tag :div, name, Keyword.delete(opts, :type)
  end
end
