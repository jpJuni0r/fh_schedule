defmodule FhScheduleWeb.CalendarLive.SearchForm do
  use Ecto.Schema
  import Ecto.Changeset

  @moduledoc """
  This schema is used for the seach function when building
  the custom calender schedule.
  """

  schema "calendar_form" do
    field :searchterm, :string, virtual: true
    field :major, :string, virtual: true
    field :selected_courses, {:array, :string}, virtual: true
  end

  def changeset(form, params) do
    form
    |> cast(params, [:searchterm, :selected_courses])
  end
end
