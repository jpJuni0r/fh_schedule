defmodule FhScheduleWeb.PageController do
  use FhScheduleWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
