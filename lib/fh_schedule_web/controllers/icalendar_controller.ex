defmodule FhScheduleWeb.ICalendarController do
  use FhScheduleWeb, :controller
  alias FhSchedule.{CourseSelection, Course}

  @tz "Europe/Berlin"
  @start_summer 12
  @start_winter 38


  def show(conn, %{"id" => id}) do
    case CourseSelection.get_course_selection(id) do
      nil ->
        text %{conn | status: 404}, "Not found"
      course_selection ->
        courses = Course.list_courses_by_names(course_selection.courses)
        text conn, to_ics(courses)
    end
  end

  def to_ics(courses) do
    start = semester_start(Timex.now(@tz))
    events =
      for course <- courses do
        for offset <- 0..14 do
          day = Timex.add(start, Timex.Duration.from_days(course.day + 7 * offset))
          %ICalendar.Event{
            summary: course.name,
            dtstart: Timex.to_datetime({{day.year, day.month, day.day}, {course.start.hour, course.start.minute, course.start.second}}, @tz),
            dtend: Timex.to_datetime({{day.year, day.month, day.day}, {course.end.hour, course.end.minute, course.end.second}}, @tz),
          }
        end
      end
      |> Enum.reduce([], fn courses, acc -> courses ++ acc end)

    %ICalendar{events: events}
    |> ICalendar.to_ics(vendor: "FH Schedule")
  end

  def semester_start(date) do
    # week of year
    week = Timex.diff(date, Timex.beginning_of_year(date), :weeks)
    semester = if week >= @start_summer && week <= @start_winter do
      :summer
    else
      :winter
    end

    case semester do
      :summer ->
        year_offset = if week >= @start_summer, do: 0, else: -1
        Timex.to_datetime({{date.year + year_offset, 4, 1}, {0, 0, 0}}, @tz)
      :winter ->
        year_offset = if week > @start_winter, do: 0, else: -1
        Timex.to_datetime({{date.year + year_offset, 10, 1}, {0, 0, 0}}, @tz)
    end
    |> Timex.add(Timex.Duration.from_weeks(1))
    |> Timex.beginning_of_week()
  end
end
