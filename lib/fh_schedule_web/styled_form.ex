defmodule FhScheduleWeb.StyledForm do
  import Phoenix.HTML.Form

  @input_class "shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
  @label_class "block text-gray-700 text-sm font-bold mb-2"

  def styled_text_input(form, field, opts \\ []) do
    text_input(form, field, opts ++ [class: @input_class])
  end

  def styled_label(form, field, text) do
    styled_label(form, field, text, [])
  end

  def styled_label(form, field, text, opts) when is_list(opts) do
    opts = Keyword.put_new(opts, :class, @label_class)
    label(form, field, text, opts)
  end
end
