defmodule FhSchedule.Crawler do
  @base_url "https://intern.fh-wedel.de/~splan/"

  @start ~T[08:00:00]
  @class_length 75
  @pause_length 15
  # [{~T[08:00:00.000000], ~T[09:15:00.000000]}, ...]
  @times 0..6
      |> Enum.map(&(Time.add(@start, &1 * (@class_length + @pause_length) * 60, :second)))
      |> Enum.map(&({&1, Time.add(&1, @class_length * 60, :second)}))


  @doc """
  Fetches name of majors and links to their timetables

  ## Example

      iex> FhSchedule.Crawler.majors()
      [
        {"Informatik (Bachelor)",
          ["http://intern.fh-wedel.de/~splan/index.html?typ=fb_fh&id=13&sem=1",
          "http://intern.fh-wedel.de/~splan/index.html?typ=fb_fh&id=13&sem=2",
          "http://intern.fh-wedel.de/~splan/index.html?typ=fb_fh&id=13&sem=3",
          ...]
        },
        ...
      ]
  """
  def majors() do
    case HTTPoison.get(@base_url) do
      {:ok, result} ->
        result.body
        |> Floki.find("table tr")
        |> Enum.drop(2)
        |> Floki.filter_out("tr td[rowspan]")
        |> Enum.map(fn row -> {
          Floki.find(row, "td:first-child") |> Floki.text() |> String.trim(),
          Floki.find(row, "a") |> Enum.map(fn anchor -> Floki.attribute(anchor, "href") |> Enum.at(0) |> sanitize_link() end)
        } end)
      {:error, error} ->
        {:error, "Fehler beim Laden des Stundenplans: #{error.reason}"}
    end
  end

  @doc """
  Get a semester calender from an URL
  """
  def cal_semester(url) do
    case HTTPoison.get(url) do
      {:ok, result} ->
        semester = semester_from_url(url)
        major = result.body |> Floki.find("h3") |> Floki.text() |> String.replace("Fachrichtung - ", "")
        calender =
          result.body
          |> Floki.find("table[border=1] > tr")
          |> Enum.drop(1)
          |> parse_rows()
        {semester, major, calender}
      {:error, error}
        -> {:error, "Fehler beim Laden des Stundenplans: #{error.reason}"}
    end

  end

  @doc """
  Removes PHPSESSID attribute from link
  """
  def sanitize_link(url) do
    parsed = URI.parse(url)
    %{ parsed | query: parsed.query
                  |> URI.query_decoder()
                  |> Enum.to_list()
                  |> Enum.filter(fn {k, _v} -> k != "PHPSESSID" end) |> URI.encode_query() }
    |> URI.to_string()
  end

  @doc """
  Gets the semester number parameter from a URL

  ## Example

      iex> FhSchedule.Crawler.semester_from_url("http://intern.fh-wedel.de/~splan/index.html?typ=fb_fh&id=13&sem=1")
      1
  """
  def semester_from_url(url) do
    {"sem", semester} =
      url
      |> URI.parse()
      |> Map.get(:query)
      |> URI.query_decoder()
      |> Enum.to_list()
      |> Enum.find(fn {k, _v} -> k == "sem" end)
    case Integer.parse(semester) do
      {num, _} -> num
      :error -> :error
    end
  end

  def parse_weekday(text) do
    ~w"Mo Di Mi Do Fr Sa So"
    |> Enum.with_index()
    |> Map.new()
    |> Map.get(text, :error)
  end

  def parse_rows(rows) do
    rows
    |> Enum.map(fn {"tr", _attr, row} ->
      Enum.map(row, fn col ->
        parse_col(col)
      end)
    end)
    |> rows_to_map()
  end

  def rows_to_map(rows) do
    empty_day = @times |> Enum.map(fn {s, e} -> {s, e, []} end)
    calendar = 0..6 |> Enum.map(&({&1, empty_day})) |> Map.new
    {filled_calender, _last_day} =
      rows
      |> Enum.reduce({calendar, 0}, fn row, {calendar, last_day} ->
        [first | rest] = row
        {day, titles} = if is_number(first), do: {first, rest}, else: {last_day, row}
        calendar = Map.update!(calendar, day, fn day_times ->
          for {{startTime, endTime, titles}, title} <- Enum.zip(day_times, titles) do
            titles = if title == "", do: titles, else: [ title | titles]
            {startTime, endTime, titles}
          end
        end)
        {calendar, day}
      end)
    filled_calender
  end

  def parse_col(col) do
    case col do
      {"td", [{"rowspan", _}], children} -> Floki.text(children) |> String.trim() |> parse_weekday()
      {"td", _, children} ->
        Floki.find(children, ".splan_veranstaltung") |> Floki.text() |> String.trim()
    end
  end
end
