defmodule FhSchedule.Course do
  use Ecto.Schema
  alias FhSchedule.{Repo, Course, Major}
  import Ecto.Changeset
  import Ecto.Query

  schema "courses" do
    field :name, :string
    field :day, :integer
    field :start, :time
    field :end, :time
    field :semester, :integer
    belongs_to :major, FhSchedule.Major

    timestamps()
  end

  @doc false
  def changeset(course, attrs) do
    course
    |> cast(attrs, [:name, :day, :start, :end])
    |> validate_required([:name, :day, :start, :end])
  end

  def get_course!(id) do
    Repo.get!(Course, id)
  end

  def list_courses() do
    Repo.all from c in Course,
        order_by: c.name,
        select: c
  end

  def list_distinct_courses() do
    Repo.all from c in Course,
        order_by: [asc: c.semester, asc: c.name],
        join: m in Major, on: m.id == c.major_id,
        select: c
  end

  def seach_courses_distinct(searchterm) do
    searchterm = String.downcase("#{searchterm}")
    Repo.all from c in Course,
        where: like(fragment("lower(?)", c.name), ^"%#{searchterm}%"),
        order_by: [asc: c.semester, asc: c.name],
        join: m in Major, on: m.id == c.major_id,
        select: c
  end

  def list_courses_by_names(course_names) when is_list(course_names) do
    Repo.all from c in Course,
        where: c.name in ^course_names,
        order_by: [asc: c.semester, asc: c.name],
        select: c
  end

  def create_course(attrs) do
    Repo.insert(attrs)
  end

  def create_courses({semester, major, calendar}) do
    now = DateTime.utc_now() |> DateTime.to_naive() |> NaiveDateTime.truncate(:second)

    # creates or gets a major and returns the id
    major_id = case Major.find_major(name: major) do
      nil ->
        {:ok, %{:id => id}} = Major.changeset(%Major{}, %{name: major}) |> Major.create_major()
        id
      %{:id => id} ->
        id
    end

    entries =
      calendar
      |> Map.to_list()
      |> Enum.map(fn {day, times} ->
        times
        |> Enum.filter(fn {_, _, list} -> length(list) != 0 end)
        |> Enum.reduce([], fn {startTime, endTime, entries}, acc ->
          courses = Enum.map(entries, &(%{
            start: Time.truncate(startTime, :second),
            end: Time.truncate(endTime, :second),
            day: day,
            semester: semester,
            name: &1,
            major_id: major_id,
            inserted_at: now,
            updated_at: now,
          }))
          [courses | acc]
        end)
        |> Enum.reduce([], &(&1 ++ &2))
      end)
      |> Enum.reduce([], &(&1 ++ &2))

    Ecto.Multi.new()
    |> Ecto.Multi.insert_all(:insert_all, Course, entries)
    |> Repo.transaction()
  end
end
