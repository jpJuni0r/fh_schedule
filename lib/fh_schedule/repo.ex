defmodule FhSchedule.Repo do
  use Ecto.Repo,
    otp_app: :fh_schedule,
    adapter: Ecto.Adapters.Postgres
end
