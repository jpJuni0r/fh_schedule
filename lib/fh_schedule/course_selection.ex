defmodule FhSchedule.CourseSelection do
  use Ecto.Schema
  alias FhSchedule.{Repo, Course, CourseSelection}
  import Ecto.Changeset
  import Ecto.Query

  schema "course_selection" do
    field :courses, {:array, :string}

    timestamps()
  end

  def changeset(course_selection, attrs) do
    course_selection
    |> cast(attrs, [:courses])
    |> validate_required([:courses])
  end

  def get_course_selection(id) do
    Repo.all(from cs in CourseSelection,
        where: cs.id == ^id,
        left_join: c in Course, on: c.name in cs.courses,
        select: cs)
    |> Enum.at(0)
  end

  def list_course_selections() do
    Repo.all from cs in CourseSelection,
        select: cs
  end

  def create_course_selection(attrs) do
    Repo.insert(attrs)
  end
end
