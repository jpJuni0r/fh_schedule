defmodule FhSchedule.Major do
  use Ecto.Schema
  alias FhSchedule.{Repo, Major}
  import Ecto.{Changeset, Repo, Query}

  schema "majors" do
    field :name, :string
    has_many :courses, FhSchedule.Course, foreign_key: :major_id

    timestamps()
  end

  def changeset(major, attrs) do
    major
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end

  def list_majors() do
    Repo.all from m in Major,
        order_by: m.name,
        select: m
  end

  def find_major(opts) do
    Repo.get_by(Major, opts)
  end

  def create_major(changeset) do
    Repo.insert(changeset)
  end

  def delete_major(major) do
    Repo.delete!(major)
  end
end
