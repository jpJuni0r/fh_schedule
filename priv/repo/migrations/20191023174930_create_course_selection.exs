defmodule FhSchedule.Repo.Migrations.CreateCousesSelection do
  use Ecto.Migration

  def change do
    create table(:course_selection) do
      add :courses, {:array, :string}

      timestamps()
    end

  end
end
