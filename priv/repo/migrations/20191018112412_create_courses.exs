defmodule FhSchedule.Repo.Migrations.CreateCourses do
  use Ecto.Migration

  def change do
    create table(:courses) do
      add :name, :string
      add :day, :integer
      add :start, :time
      add :end, :time
      add :major_id, references(:majors, on_delete: :nothing)

      timestamps()
    end

    create index(:courses, [:major_id])
  end
end
