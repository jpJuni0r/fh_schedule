defmodule FhSchedule.Repo.Migrations.AddSenester do
  use Ecto.Migration

  def change do
    alter table(:courses) do
      add :semester, :integer
    end

  end
end
