# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     FhSchedule.Repo.insert!(%FhSchedule.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias FhSchedule.{Repo, Crawler, Course}

# Delete all `Course`s

Repo.delete_all(Course)

# Create `Course` entries for Bachelor Informatik
for semester <- 1..7 do
  url = "http://intern.fh-wedel.de/~splan/index.html?typ=fb_fh&id=13&sem=#{semester}"
  cal = Crawler.cal_semester(url)
  Course.create_courses(cal)
end

